<?php

use yii\db\Migration;
use yii\db\Schema;

class m000000_000006_universalmailtemplate extends Migration
{

    public function up()
    {
        $this->createTable('universal_mail_template',[
            'id' => Schema::TYPE_PK,
            'name' =>  Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'subject' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'text' => Schema::TYPE_TEXT . ' DEFAULT NULL',
            'type' => Schema::TYPE_INTEGER .  " DEFAULT NULL",
            'class_name' => Schema::TYPE_INTEGER .  " DEFAULT NULL",
        ], 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');
    }

    public function safeDown()
    {   
        $this->dropTable('universal_mail_template');
        
        echo "m000000_000006_universalmailtemplate was deleted.\n";
         
        return true;
    }

}
