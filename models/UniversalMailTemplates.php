<?php

namespace hdmodules\universalmailtemplate\models;

use Yii;
use yii\db\ActiveRecord;

use hdmodules\base\multilanguage\MultiLanguageBehavior;
use hdmodules\base\multilanguage\MultiLanguageTrait;


class UniversalMailTemplates extends ActiveRecord {

	use MultiLanguageTrait;

	const TYPE_ADMIN = 1;
	const TYPE_USER = 2;
	const TYPE_NULL = 0;

	public static $type = [
		"null" => self::TYPE_NULL,
		"mailAdmin" => self::TYPE_ADMIN,
		"mailClient" => self::TYPE_USER
	];


	public static function tableName() {
		return "universal_mail_template";
	}

	public function rules() {
		return [
            [['name','subject'],'required'],
			[['name','subject','text', 'class_name'],'trim'],
			[['name','subject','text','type', 'class_name'],'safe']
		];
	}

	public function attributeLabels() {
		return [
			'id' => 'ID',
			'name' => Yii::t('mail-template','Name'),
			'subject' => Yii::t('mail-template','Subject'),
			'text' => Yii::t('mail-template','Text'),
			'type' => Yii::t('mail-template','Type'),
		];
	}

	public function behaviors() {
		return [
			'mlBehavior' => [
				'class' => MultiLanguageBehavior::className(),
				'mlConfig' => [
					'db_table' => 'translations_with_string',
					'attributes' => ['subject', 'text'],
					'admin_routes' => [
						'admin/*'
					],
				],
			],
		];
	}

	public static function sendEmail($email, $attributes, $class, $model) {

		Yii::$app->language = $model->lang ? $model->lang : 'en';

		$templates = UniversalMailTemplates::find()->where(["class_name" => $class, 'type'=>['mailAdmin', 'mailClient']])->all();

		if(method_exists($model,"specialTemplateAttributes")) {
			$special = $model->specialTemplateAttributes();
			foreach($special as $attribute => $value) {
				$model->$attribute = $value;
			}
		}

		foreach($templates as $template) {
			$message = $template->text;

			foreach($attributes as $attribute) {
				if($model->{$attribute} && !empty($model->{$attribute})){
					$message = str_replace('{{'.$attribute.'}}',trim($model->{$attribute}),$message);
				}else{
					$message = str_replace('{{'.$attribute.'}}', '' ,$message);
				}
			}

			$message = preg_replace('@<code.*pointer.*>(.*)</code>@m','$1',$message);

			$subject = $template->subject;

			if(self::$type[$template->type] == self::TYPE_USER) {

				Yii::$app->mailer->compose()
					->setTo($email)
					->setFrom("noreply@slotegrator.com")
					->setSubject($subject)
					->setHtmlBody($message)
					->send();

			} else if(self::$type[$template->type] == self::TYPE_ADMIN) {

				foreach(Setting::get('admin_email') as $mail) {
					Yii::$app->mailer->compose()
						->setTo($mail)
						->setFrom("noreply@slotegrator.com")
						->setSubject($subject)
						->setHtmlBody($message)
						->send();
				}
			}
		}
	}
}