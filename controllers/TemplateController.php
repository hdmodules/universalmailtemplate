<?php

namespace hdmodules\universalmailtemplate\controllers;

use Yii;
use hdmodules\base\controllers\Controller;
use hdmodules\universalmailtemplate\models\UniversalMailTemplates;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;


class TemplateController extends Controller
{

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => UniversalMailTemplates::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionCreate()
    {
        $model = new UniversalMailTemplates();

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }else{
                if ($model->save()) {
                    $this->flash('success', Yii::t('mail-template', 'Template create'));
                    return $this->redirect(['index']);
                } else {
                    $this->flash('error', Yii::t('mail-template', 'Update error. {0}', $model->formatErrors()));
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else {
                if ($model->save()) {
                    $this->flash('success', Yii::t('mail-template', 'Template updated'));
                    return $this->redirect(['index']);
                } else {
                    $this->flash('error', Yii::t('mail-template', 'Update error. {0}', $model->formatErrors()));
                }
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);

    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = UniversalMailTemplates::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
