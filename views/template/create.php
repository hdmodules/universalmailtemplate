<?php
$this->title = Yii::t('mail-template', 'Create UniversalMailTemplate');
?>

<?= $this->render('_menu') ?>

<?= $this->render('_form', [
    'model' => $model
]) ?>