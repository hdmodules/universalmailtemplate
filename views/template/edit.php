<?php
$this->title = Yii::t('mail-template', 'Update UniversalMailTemplate');
?>

<?= $this->render('_menu') ?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>