<?php
use yii\grid\GridView;
use  \hdmodules\universalmailtemplate\models\UniversalMailTemplates;

$this->title = Yii::t('mail-template', 'UniversalMailTemplate');
$module = $this->context->module->id;
?>

<?= $this->render('_menu') ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute'=>'id',
            'headerOptions' => ['width' => '40'],
        ],
        [
            'attribute'=>'name',
            'content'=>function($data){
                return \yii\helpers\Html::a($data->name, \yii\helpers\Url::toRoute(['update', 'id'=>$data->id]));
            }

        ],
        'subject',
        [
            'attribute' => 'type',
            'content' => function($data) {
                switch($data->type) {
                    case UniversalMailTemplates::TYPE_NULL:
                        return "&lt;none&gt;";
                    case UniversalMailTemplates::TYPE_USER:
                        return "Email for user";
                    case UniversalMailTemplates::TYPE_ADMIN:
                        return "Email for admin";
                }
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header'=>Yii::t('mail-template', 'Actions'),
            'headerOptions' => ['width' => '80'],
            'template'=>'{view} {update} {delete}'
        ],
    ],
]) ?>