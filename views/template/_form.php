<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use hdmodules\base\multilanguage\input_widget\MultiLanguageActiveField;
use hdmodules\base\widgets\RedactorMultiLanguageInput;

$module = $this->context->module->id;
$relation_classes = $this->context->module->relation_classes;
$action = $this->context->action->id;
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h2><?= Yii::t('request', 'Form') ?> <small><?= Yii::t('mail-template', 'create UniversalMailTemplate') ?></small></h2>
                <ul class="nav navbar-right">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'validateOnSubmit' => true,
                ]); ?>

                <?= $form->field($model, 'name') ?>

                <?= $form->field($model, 'type')->dropDownList([$model::TYPE_NULL => "<none>",$model::TYPE_USER => "Email for user", $model::TYPE_ADMIN => "Email for admin"],[])?>

                <?= $form->field($model, 'subject')->widget(MultiLanguageActiveField::className()) ?>

                <?php if(Yii::$app->controller->action->id == "create") { ?>
                    <div style="margin:10px 0 10px 0;" >
                        <strong>You can add TEXT after the form is saved.</strong>
                    </div>
                <?php } else { ?>

                    <?php
                    if($model->class_name){
                        $runtime = new $model->class_name;
                        $attributes = $runtime->getTemplateAttributes();
                    }
                    ?>

                    <?php if(isset($attributes)){ ?>

                        <div style="margin:10px 0 10px 0;" >
                            <strong>You can use these special values in text:</strong>
                        </div>

                        <table class="template-attributes" style="margin-bottom: 20px">
                            <?php foreach($attributes as $attr => $desc) { ?>
                                <tr>
                                    <td><code>{{<?=$attr?>}}</code></td>
                                    <td><?=$desc?></td>
                                </tr>
                            <?php } ?>

                        </table>
                    <?php } ?>


                    <?= RedactorMultiLanguageInput::widget($model, 'text', ['options' => [
                        'minHeight' => 400,
                        'imageUpload' => Url::to(['/redactor/upload', 'dir' => 'mail_templates']),
                        'fileUpload' => Url::to(['/redactor/upload', 'dir' => 'mail_templates']),
                        'plugins' => ['fullscreen']
                    ]]); ?>

                    <br>

                <?php } ?>


                <?= $form->field($model, 'class_name')->dropDownList(array_combine($relation_classes,$relation_classes))?>

                <div class="ln_solid"></div>

                <?= Html::submitButton(Yii::t('mail-template', 'Save'), ['class' => 'btn btn-primary']) ?>

                <?php ActiveForm::end(); ?>

            </div>

        </div>
    </div>
</div>

