<?php
use yii\helpers\Url;

$module     = $this->context->module->id;
$controller = Yii::$app->controller->id;
$action     = $this->context->action->id;

?>

<ul class="nav nav-pills">
    <li <?= ($action == 'index') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['index']) ?>">
            <?php if($action != 'index') echo '<i class="glyphicon glyphicon-chevron-left font-12"></i> ' ?>
            <?= Yii::t('mail-template', 'All') ?>
        </a>
    </li>

    <?php if($action == 'update') { ?>
        <li <?= ($action == 'update') ? 'class="active"' : '' ?>>
            <a>
                <?= Yii::t('mail-template', 'Update') ?>
            </a>
        </li>
    <?php } ?>

    <li <?= ($action == 'create') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['create']) ?>">
            <?= Yii::t('mail-template', 'Add') ?>
        </a>
    </li>
</ul>
<br/>